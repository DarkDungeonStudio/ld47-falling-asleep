﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextOverTime : MonoBehaviour
{
    public float TextDelay;
    public TextMeshProUGUI TextBox;

    private string FullText;
    private string currentText;


    private void Start()
    {
        FullText = TextBox.text;
        TextBox.text = "";
        StartCoroutine(ShowText());
    }

    IEnumerator ShowText()
    {
        for(int i = 0; i < FullText.Length; i++)
        {
            yield return new WaitForSeconds(TextDelay);
            currentText += FullText[i];
            TextBox.text = currentText;
        }
    }
}
