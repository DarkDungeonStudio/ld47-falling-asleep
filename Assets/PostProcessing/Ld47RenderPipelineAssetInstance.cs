﻿using UnityEngine;
using UnityEngine.Rendering;

public class Ld47RenderPipelineAssetInstance : RenderPipeline
{
    public Ld47RenderPipelineAssetInstance()
    {
    }

    // Unity calls this method once per frame for each CameraType that is currently rendering.
    protected override void Render(ScriptableRenderContext context, Camera[] cameras)
    {
        // This is where you can write custom rendering code. Customize this method to customize your SRP.
    }
}