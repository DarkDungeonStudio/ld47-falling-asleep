﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioListenerVolume : MonoBehaviour
{
    public AudioListener AudioListener;

    // Update is called once per frame
    void Update()
    {
        AudioListener.volume = GameManager.Instance.MainVolume;
    }
}
