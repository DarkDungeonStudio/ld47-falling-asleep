﻿using UnityEditor.Rendering.PostProcessing;

[PostProcessEditor(typeof(OilPainting))]
public sealed class OilPaintingEditor : PostProcessEffectEditor<OilPainting>
{
    SerializedParameterOverride m_Range;

    public override void OnEnable()
    {
        m_Range = FindParameterOverride(x => x.radius);
    }

    public override void OnInspectorGUI()
    {
        PropertyField(m_Range);
    }
}