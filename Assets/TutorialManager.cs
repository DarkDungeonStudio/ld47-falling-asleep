﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class TutorialManager : MonoBehaviour
{
    public List<UnityEvent> Actions;

    public GameObject Object1ToMerge;
    public GameObject Object2ToMerge;
    public GameObject DummyObject;

    private bool TutorialFinished = false;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && Actions.Count > 0 && (!Object1ToMerge || !Object1ToMerge.activeInHierarchy) && (!Object2ToMerge || !Object2ToMerge.activeInHierarchy))
        {
            Actions.First().Invoke();
            Actions.RemoveAt(0);
        }

        if(!Object1ToMerge && !Object2ToMerge && !TutorialFinished)
        {
            Actions.First().Invoke();
            Actions.RemoveAt(0);
            TutorialFinished = true;
        }        
    }
}
