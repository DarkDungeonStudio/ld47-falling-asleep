﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    public Animator Animator;
    public string Trigger;
    public LoadScene LoadScene;
    public int SceneToLoad;
    public float Delay = 2f;

    private void Awake()
    {
        Animator.SetTrigger("Wakeup");
        StartCoroutine(LoadMainGame());
    }

    IEnumerator LoadMainGame()
    {
        yield return new WaitForSeconds(Delay);
        LoadScene.LoadNewScene(SceneToLoad);
    }
}
