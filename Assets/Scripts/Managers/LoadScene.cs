﻿using System.Collections;
using System.Runtime.Serialization;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadScene : MonoBehaviour
{
    [OptionalField]
    public Animator transition;
    public float transitionTime = 1f;
    [OptionalField]
    public Slider Slider;
    [OptionalField]
    public TextMeshProUGUI SliderText;

    private void Awake()
    {
        transition.gameObject.SetActive(true);
    }

    public void LoadNewScene(int sceneIndex)
    {
        if(Time.timeScale < 1)
        {
            Time.timeScale = 1;
            AudioListener.pause = false;
            PauseGameManager.isGamePaused = false;
        }

        StartCoroutine(LoadLevel(sceneIndex));
    }

    IEnumerator LoadLevel(int sceneIndex)
    {
        if (Slider)
        {
            Slider.value = 0;
            if (SliderText)
            {
                SliderText.text = $"{0}%";
            }
        }

        if (transition)
        {
            transition.SetTrigger("Start");
            yield return new WaitForSeconds(transitionTime);
        }
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        if (Slider)
        {
            while (!operation.isDone)
            {
                var progress = (int)(Mathf.Clamp01(operation.progress / 0.9f) * 100);
                Slider.value = progress;
                if (SliderText)
                {
                    SliderText.text = $"{progress}%";
                }
                yield return null;
            }
        }
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
          Application.Quit();
#endif
    }
}
