﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashScreen : MonoBehaviour
{
    public int sceneIndex = 1;
    public float delayTime = 2f;
    public LoadScene LoadSceneManager;

    private void Update()
    {
        if(Input.anyKey)
        {
            LoadSceneManager.LoadNewScene(sceneIndex);
        }
    }

    void Start()
    {
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(delayTime);
        LoadSceneManager.LoadNewScene(sceneIndex);
    }
}
