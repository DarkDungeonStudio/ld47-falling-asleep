﻿using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton Setup
    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    [Range(0f,1f)]
    public float MainVolume = 1f;
    [Range(0f, 1f)]
    public float ScreamVolume = 0.2f;
    [Range(0f, 1f)]
    public float EffectsVolume = 1f;

    public int LoopCount = 0;
    public int BobbyMaxMentalHealth = 25;
    public int BobbyMentalHealth = 25;

    public bool GameOver = false;

    private void Update()
    {
        if(BobbyMentalHealth <= 0 && !GameOver)
        {
            GameOver = true;
            FindObjectOfType<LoadScene>().LoadNewScene(5);
        }
    }
}
