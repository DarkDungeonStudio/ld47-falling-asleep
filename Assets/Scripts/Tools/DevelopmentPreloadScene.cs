﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DevelopmentPreloadScene : MonoBehaviour
{
    private void Awake()
    {
        GameObject preload = GameObject.Find("GameManager");

        if(preload == null)
        {
            SceneManager.LoadScene("_preload");
        }
    }
}
