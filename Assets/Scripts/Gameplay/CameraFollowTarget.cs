﻿using UnityEngine;

public class CameraFollowTarget : MonoBehaviour
{
    public Transform target;

    [Range(0, 10)]
    public float MoveDelay = 5f;

    public Vector3 DesiredOffset;

    // Update is called once per frame
    void LateUpdate()
    {
        var newPos = target.position + DesiredOffset;

        var smoothPos = Vector3.Lerp(transform.position, newPos, MoveDelay * Time.deltaTime);

        transform.position = smoothPos;
    }
}
