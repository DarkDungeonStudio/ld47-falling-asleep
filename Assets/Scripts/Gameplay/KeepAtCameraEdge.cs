﻿using UnityEngine;

public class KeepAtCameraEdge : MonoBehaviour
{
    public Vector2 Offset;

    void Update()
    {
        transform.position = Camera.main.ViewportToWorldPoint(new Vector3(Offset.x, Offset.y, Mathf.Abs(Camera.main.transform.position.z - transform.position.z)));
    }
}
