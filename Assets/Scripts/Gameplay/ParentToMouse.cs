﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentToMouse : MonoBehaviour
{
    public float LerpSpeed = 5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        var pos = new Vector3(mouse.x, mouse.y, transform.position.z);
        
        transform.position = Vector3.Lerp(transform.position, pos, LerpSpeed * Time.deltaTime);
    }
}
