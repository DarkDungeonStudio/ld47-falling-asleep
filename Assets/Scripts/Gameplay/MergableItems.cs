﻿using System.Collections.Generic;

public class MergableItem
{
    public string ItemName { get; set; }
    public string RequiredItem1 { get; set; }
    public string RequiredItem2 { get; set; }
}

public static class MergableItems
{
    public static MergableItem Pillow = new MergableItem
    {
        ItemName = "Pillow"
    };
    public static MergableItem Blanket = new MergableItem
    {
        ItemName = "Blanket"
    };
    public static MergableItem Mattress = new MergableItem
    {
        ItemName = "Mattress"
    }; 
    public static MergableItem BedBase = new MergableItem
    {
        ItemName = "BedBase"
    };
    public static MergableItem UnmadeBed = new MergableItem
    {
        ItemName = "UnmadeBed",
        RequiredItem1 = "Mattress",
        RequiredItem2 = "BedBase"
    };
    public static MergableItem Bedding = new MergableItem
    {
        ItemName = "Bedding",
        RequiredItem1 = "Pillow",
        RequiredItem2 = "Blanket"
    };
    public static MergableItem MadeBed = new MergableItem
    {
        ItemName = "MadeBed",
        RequiredItem1 = "Bedding",
        RequiredItem2 = "UnmadeBed"
    };
}

public static class MergableItemList
{
    public static List<MergableItem> MergableItemInList = new List<MergableItem>
    {
        MergableItems.Pillow,
        MergableItems.Blanket,
        MergableItems.Bedding,
        MergableItems.Mattress,
        MergableItems.BedBase,
        MergableItems.UnmadeBed,
        MergableItems.MadeBed
    };
}