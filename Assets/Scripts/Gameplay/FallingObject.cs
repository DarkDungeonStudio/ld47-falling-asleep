﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingObject : MonoBehaviour
{
    public Vector2 SpawnOffset;
    public AudioSource soundEffectSource; // On interact sound

    public float StartDelay = 1f;

    public int Hardness = 1;

    private bool isGrabbed = false;
    private Rigidbody2D Rigidbody2D;


    private void Start()
    {
        Rigidbody2D = GetComponent<Rigidbody2D>();
        Rigidbody2D.gravityScale = 0;
        StartCoroutine(StartDelayMethod());
        ResetObjectToTop();

        var newPos = transform.position;

        newPos.x = Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(0.1f,0.9f), SpawnOffset.y, Camera.main.transform.position.z)).x;

        transform.position = newPos;
    }

    IEnumerator StartDelayMethod()
    {
        yield return new WaitForSeconds(Random.Range(0, StartDelay));
        Rigidbody2D.gravityScale = 1;
        //Rigidbody2D.AddTorque(Random.Range(-2.5f, 2.5f), ForceMode2D.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        //if (!isGrabbed)
        //    return;

        if (Input.GetKeyUp(KeyCode.Mouse0) && GrabItem.GameObject1 == gameObject)
        {
            ReleaseObject();
            GrabItem.GameObject1 = null;
        }

        if (Input.GetKeyUp(KeyCode.Mouse1) && GrabItem.GameObject2 == gameObject)
        {
            ReleaseObject();
            GrabItem.GameObject2 = null;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bobby") && !gameObject.name.Equals(MergableItems.MadeBed.ItemName))
        {
            // depending on the item that hits bobby, he might get injured or another effect
            GameManager.Instance.BobbyMentalHealth -= Hardness;
            ReleaseObject();

            if (GrabItem.GameObject1 == gameObject)
                GrabItem.GameObject1 = null;

            if (GrabItem.GameObject2 == gameObject)
                GrabItem.GameObject2 = null;

            var rigidbody = collision.gameObject.GetComponent<Rigidbody2D>();
            rigidbody.velocity = Vector2.zero;
            rigidbody.AddForce(Vector2.up * 10f, ForceMode2D.Impulse);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("OffCamera"))
        {
            ResetObjectToTop();
        }
    }

    private void ResetObjectToTop()
    {
        // move object above camera, maintain velocity
        var pos = Camera.main.ViewportToWorldPoint(new Vector3(SpawnOffset.x, SpawnOffset.y, Mathf.Abs(Camera.main.transform.position.z - transform.position.z)));
        pos.x = transform.position.x;
        transform.position = pos;
    }

    private void OnMouseOver()
    {
        if (isGrabbed)
            return;

        if (Input.GetKeyDown(KeyCode.Mouse0) && !GrabItem.GameObject1)
        {
            GrabObject();
            GrabItem.GameObject1 = gameObject;
        }

        if (Input.GetKeyDown(KeyCode.Mouse1) && !GrabItem.GameObject2)
        {
            GrabObject();
            GrabItem.GameObject2 = gameObject;
        }
    }

    public void GrabObject()
    {
        isGrabbed = true;
        Rigidbody2D.gravityScale = 0;
        Rigidbody2D.velocity = new Vector2();

        if (soundEffectSource)
        {
            soundEffectSource.Play();
        }

        gameObject.layer = 11;
    }

    public void ReleaseObject()
    {
        isGrabbed = false;
        Rigidbody2D.gravityScale = 1;
        gameObject.layer = 10;
    }
}
