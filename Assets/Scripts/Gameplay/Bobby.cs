﻿using System.Collections;
using UnityEngine;

public class Bobby : MonoBehaviour
{
    public Vector2 SpawnOffset;
    public float StartDelay = 1f;
    public AudioSource screaming;
    [Range(0f, 1f)]
    public float maxVolume = 0.8f;
    [Range(0f, 1f)]
    public float minVolume = 0.1f;

    public LoadScene LoadScene;

    private Rigidbody2D Rigidbody2D;

    private void Start()
    {
        maxVolume = GameManager.Instance.ScreamVolume;
        Rigidbody2D = GetComponent<Rigidbody2D>();
        Rigidbody2D.gravityScale = 0;
        StartCoroutine(StartDelayMethod());

        ResetBackToTop();
    }
    IEnumerator StartDelayMethod()
    {
        yield return new WaitForSeconds(Random.Range(0, StartDelay));
        Rigidbody2D.gravityScale = 1;
    }

    private void Update()
    {
        maxVolume = GameManager.Instance.ScreamVolume;
        Vector3 viewPos = Camera.main.WorldToViewportPoint(transform.position);
        float volume = Mathf.Clamp((maxVolume / 10) / Mathf.Abs(viewPos.y - 0.5f), minVolume, maxVolume);
        screaming.volume = volume;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("OffCamera"))
        {
            ResetBackToTop();
            GameManager.Instance.LoopCount++;
            GameManager.Instance.BobbyMentalHealth--;
        }

        if (collision.gameObject.name.Equals(MergableItems.MadeBed.ItemName))
        {
            LoadScene.LoadNewScene(6);
        }
    }

    private void ResetBackToTop()
    {
        var pos = Camera.main.ViewportToWorldPoint(new Vector3(SpawnOffset.x, SpawnOffset.y, Mathf.Abs(Camera.main.transform.position.z - transform.position.z)));
        pos.x = transform.position.x;
        transform.position = pos;
    }
}
