﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GrabItem : MonoBehaviour
{
    public GameObject GameObject1MouseParent;
    public GameObject GameObject2MouseParent;

    public static GameObject GameObject1;
    public static GameObject GameObject2;

    public float LerpSpeed = 10f;

    public AudioSource CombineSFX;
    public ParticleSystem ComineVFX;
    public GameObject[] PrefabsToSpawn;

    void Update()
    {
        if (GameObject1)
        {
            GameObject1.transform.position = Vector3.Lerp(GameObject1.transform.position, GameObject1MouseParent.transform.position, LerpSpeed * Time.deltaTime);
        }

        if (GameObject2)
        {
            GameObject2.transform.position = Vector3.Lerp(GameObject2.transform.position, GameObject2MouseParent.transform.position, LerpSpeed * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.Space) && GameObject1 && GameObject2)
        {
            var names = new List<string>
            {
                GameObject1.name,
                GameObject2.name
            };

            // Can objects be combined?
            //// No -> Release objects with random force to left/right
            //// Play sound effect on failure
            var itemToMake = MergableItemList.MergableItemInList.SingleOrDefault(x => names.Contains(x.RequiredItem1) && names.Contains(x.RequiredItem2));
            if (itemToMake == null)
            {
                Debug.Log("Invalid recipe!");
                GameObject1.GetComponent<FallingObject>().ReleaseObject();
                GameObject2.GetComponent<FallingObject>().ReleaseObject();
                GameObject1.GetComponent<Rigidbody2D>().AddForce(Vector2.left * Random.Range(5,10), ForceMode2D.Impulse);
                GameObject2.GetComponent<Rigidbody2D>().AddForce(Vector2.right * Random.Range(5, 10), ForceMode2D.Impulse);
                GameObject1 = null;
                GameObject2 = null;
                return;
            }

            // Combine game objects
            //// Remove two objects from the game, and replace them with a new object in their place
            //// Wil be help in GameObject1MouseParent
            //// Play sound effect on combine
            Destroy(GameObject1);
            Destroy(GameObject2);
            GameObject1 = null;
            GameObject2 = null;
            var newObject = Instantiate(PrefabsToSpawn.SingleOrDefault(x => x.name == itemToMake.ItemName), transform.position, Quaternion.identity);
            newObject.name = itemToMake.ItemName;
            CombineSFX.Play();
            ComineVFX.Play();
        }
    }
}
