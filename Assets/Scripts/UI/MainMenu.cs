﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public AudioSource SoundEffectsSource;
    public List<AudioClip> SoundEffects;

    public Slider Difficulty;
    public Slider MainVolume;
    public Slider ScreamVolume;
    public Slider EffectsVolume;

    private void Awake()
    {
        Difficulty.value = GameManager.Instance.BobbyMaxMentalHealth;
        MainVolume.value = GameManager.Instance.MainVolume;
        ScreamVolume.value = GameManager.Instance.ScreamVolume;
        EffectsVolume.value = GameManager.Instance.EffectsVolume;

        GameManager.Instance.BobbyMentalHealth = GameManager.Instance.BobbyMaxMentalHealth;
        GameManager.Instance.LoopCount = 0;
        GameManager.Instance.GameOver = false;
    }

    public void PlayButtonClickEffect()
    {
        if (SoundEffects.Count > 0)
        {
            SoundEffectsSource.clip = SoundEffects[Random.Range(0, SoundEffects.Count - 1)];
            SoundEffectsSource.Play();
        }
    }

    public void UpdateVolume()
    {
        GameManager.Instance.BobbyMaxMentalHealth = Mathf.RoundToInt(Difficulty.value);
        GameManager.Instance.BobbyMentalHealth = Mathf.RoundToInt(Difficulty.value);
        GameManager.Instance.MainVolume = MainVolume.value;
        GameManager.Instance.ScreamVolume = ScreamVolume.value;
        GameManager.Instance.EffectsVolume = EffectsVolume.value;
    }
}
