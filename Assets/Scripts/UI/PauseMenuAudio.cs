﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuAudio : MonoBehaviour
{
    public AudioSource SoundEffectsSource;
    public AudioSource MusicSource;
    public Slider MainVolume;
    public Slider ScreamVolume;
    public Slider EffectsVolume;

    public List<AudioClip> SoundEffects;

    private void Awake()
    {
        SoundEffectsSource.ignoreListenerPause = true;
        MusicSource.ignoreListenerPause = true;

        MainVolume.value = GameManager.Instance.MainVolume;
        
        if(ScreamVolume)
            ScreamVolume.value = GameManager.Instance.ScreamVolume;

        EffectsVolume.value = GameManager.Instance.EffectsVolume;
    }

    public void PlayButtonClickEffect()
    {
        if (SoundEffects.Count > 0)
        {
            SoundEffectsSource.clip = SoundEffects[Random.Range(0, SoundEffects.Count - 1)];
            SoundEffectsSource.Play();
        }
    }

    public void UpdateVolume()
    {
        GameManager.Instance.MainVolume = MainVolume.value;
        
        if(ScreamVolume)
            GameManager.Instance.ScreamVolume = ScreamVolume.value;

        GameManager.Instance.EffectsVolume = EffectsVolume.value;
    }
}
