﻿using UnityEngine;

public class PauseGameManager : MonoBehaviour
{
    public GameObject PauseMenu;

    public static bool isGamePaused = false;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isGamePaused = !isGamePaused;
            Time.timeScale = isGamePaused ? 0 : 1;
            PauseMenu.SetActive(isGamePaused);
            AudioListener.pause = isGamePaused;
        }
    }

    public void ResumeGame()
    {
        isGamePaused = false;
        Time.timeScale = 1;
        PauseMenu.SetActive(isGamePaused);
        AudioListener.pause = isGamePaused;
    }
}
