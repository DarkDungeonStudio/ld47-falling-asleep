﻿using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess(typeof(OilPaintingRenderer), PostProcessEvent.AfterStack, "Custom/OilPainting")]
public sealed class OilPainting : PostProcessEffectSettings
{
    [Range(0, 10), Tooltip("OilPainting effect intensity.")]
    public FloatParameter radius = new FloatParameter { value = 10 };
}

public sealed class OilPaintingRenderer : PostProcessEffectRenderer<OilPainting>
{
    public override void Render(PostProcessRenderContext context)
    {
        var sheet = context.propertySheets.Get(Shader.Find("Unlit/OilPainting"));
        sheet.properties.SetFloat("_Radius", settings.radius);
        context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
    }
}