﻿using UnityEngine;

public class EffectsVolume : MonoBehaviour
{
    public AudioSource AudioSource;

    // Update is called once per frame
    void Update()
    {
        AudioSource.volume = GameManager.Instance.EffectsVolume;
    }
}
