﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intro : MonoBehaviour
{
    public Animator Animator;
    public string Trigger;
    public LoadScene LoadScene;
    public int SceneToLoad;
    public float Delay = 15f;

    private bool Loading = false;

    private void Start()
    {
        if (!string.IsNullOrEmpty(Trigger))
        {
            Animator.SetTrigger(Trigger);
        }

        StartCoroutine(LoadMainGame());
    }

    IEnumerator LoadMainGame()
    {
        yield return new WaitForSeconds(Delay);
        Loading = true;
        LoadScene.LoadNewScene(SceneToLoad);
    }

    private void Update()
    {
        if(Input.anyKeyDown && !Loading)
        {
            LoadScene.LoadNewScene(SceneToLoad);
            Loading = true;
        }
    }
}
