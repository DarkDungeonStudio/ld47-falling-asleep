﻿using TMPro;
using UnityEngine;

public class UpdateLoopCount : MonoBehaviour
{
    public TextMeshProUGUI LoopCountScore;
    public TextMeshProUGUI BobbyHealth;

    private void Update()
    {
        LoopCountScore.text = GameManager.Instance.LoopCount.ToString();
        BobbyHealth.text = GameManager.Instance.BobbyMentalHealth.ToString();
    }
}
